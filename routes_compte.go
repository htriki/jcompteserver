package main

import (
	"log"
	"net/http"
	"strconv"

	"github.com/gorilla/mux"
)

type jsonCompte struct {
	Id        int64  `json:"id"`
	Libelle   string `json:"libelle"`
	Titulaire string `json:"titulaire"`
	Numero    string `json:"numero"`
	Actif     bool   `json:"actif"`
}
type jsonCompteComplete struct {
	Id        int64   `json:"id"`
	Libelle   string  `json:"libelle"`
	Titulaire string  `json:"titulaire"`
	Numero    string  `json:"numero"`
	Actif     bool    `json:"actif"`
	Total     float32 `json:"total"`
	TotalReel float32 `json:"totalReel"`
}

func (s *server) handleCompte() http.HandlerFunc {

	return func(w http.ResponseWriter, r *http.Request) {

		s.enableCors(&w)

		comptes, err := s.store.findCompte()

		if err != nil {
			s.manageError(err, "handleCompte : Pb de récupération des comptes", w, r, http.StatusInternalServerError)
			return
		}

		var resp = make([]jsonCompteComplete, len(comptes))
		for i, c := range comptes {
			resp[i] = mapCompteToJsonComplete(c)
		}

		s.respond(w, r, resp, http.StatusOK)

	}
}

func (s *server) handleCompteActif() http.HandlerFunc {

	return func(w http.ResponseWriter, r *http.Request) {

		s.enableCors(&w)

		comptes, err := s.store.findCompteActif()

		if err != nil {
			s.manageError(err, "handleCompteActif : Pb de récupération des comptes actifs", w, r, http.StatusInternalServerError)
			return
		}

		var resp = make([]jsonCompte, len(comptes))
		for i, c := range comptes {
			resp[i] = mapCompteToJson(c)
		}

		s.respond(w, r, resp, http.StatusOK)
	}
}

func (s *server) handleCompteDetail() http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {

		s.enableCors(&w)

		vars := mux.Vars(r)
		id, err := strconv.ParseInt(vars["id"], 10, 64)
		if err != nil {
			s.manageError(err, "handleCompteDetail : Pb de parse du parametre", w, r, http.StatusBadRequest)
			return
		}
		compte, err := s.store.findCompteById(id)

		if err != nil {
			s.manageError(err, "handleCompteDetail : Compte inconnu ", w, r, http.StatusNotFound)
			return
		}

		var resp = mapCompteToJson(compte)

		s.respond(w, r, resp, http.StatusOK)
	}
}

func (s *server) handleCompteCreate() http.HandlerFunc {

	type request struct {
		Libelle   string `json:"libelle"`
		Titulaire string `json:"titulaire"`
		Numero    string `json:"numero"`
		Actif     bool   `json:"actif"`
	}
	return func(w http.ResponseWriter, r *http.Request) {

		s.enableCors(&w)

		req := request{}
		err := s.decode(w, r, &req)
		if err != nil {
			s.manageError(err, "handleCompteCreate : Impossible de parse le compte ", w, r, http.StatusBadRequest)
			return
		}

		// Créer un compte
		c := &Compte{
			Id:        0,
			Libelle:   req.Libelle,
			Titulaire: req.Titulaire,
			Numero:    req.Numero,
			Actif:     req.Actif,
		}

		s.store.BeginTx()

		// Sauvegarde du compte
		err = s.store.createCompte(c)

		if err != nil {
			s.manageErrorWithRollback(err, "handleCompteCreate : Impossible de créer le compte", w, r, http.StatusInternalServerError)
			return
		}

		s.store.Commit()

		var resp = mapCompteToJson(c)
		s.respond(w, r, resp, http.StatusOK)
	}
}

func (s *server) handleCompteDelete() http.HandlerFunc {

	return func(w http.ResponseWriter, r *http.Request) {

		s.enableCors(&w)

		vars := mux.Vars(r)
		id, err := strconv.ParseInt(vars["id"], 10, 64)
		if err != nil {
			s.manageError(err, "handleCompteDelete : Impossible de parse l'id' ", w, r, http.StatusBadRequest)
			return
		}

		s.store.BeginTx()

		countOperation, err := s.store.countOperationByCompte(id)

		if err != nil {
			s.manageErrorWithRollback(err, "handleCompteDelete : Erreur pour lire le total des opérations par compte", w, r, http.StatusInternalServerError)
			return
		}

		if countOperation > 0 {
			s.manageErrorWithRollback(err, "handleCompteDelete : Impossible de supprimer s'il existe des opérations", w, r, http.StatusBadRequest)
			return
		}

		count, err := s.store.deleteCompte(id)
		if err != nil {
			s.manageErrorWithRollback(err, "handleCompteDelete : Erreur lors de la suppression du compte", w, r, http.StatusInternalServerError)
			return
		}

		s.store.Commit()

		log.Printf("nombre de ligne supprimée %v", count)

		s.respond(w, r, count, http.StatusOK)
	}
}

func (s *server) handleCompteUpdate() http.HandlerFunc {

	return func(w http.ResponseWriter, r *http.Request) {

		s.enableCors(&w)

		req := jsonCompte{}
		err := s.decode(w, r, &req)

		if err != nil {
			s.manageError(err, "handleCompteUpdate : Impossible de parse le compte", w, r, http.StatusBadRequest)
			return
		}

		compte := &Compte{
			Id:        req.Id,
			Libelle:   req.Libelle,
			Titulaire: req.Titulaire,
			Numero:    req.Numero,
			Actif:     req.Actif,
			//Tags:          tags,
		}

		s.store.BeginTx()

		// Mise à jour du compte
		err = s.store.updateCompte(compte)

		if err != nil {
			s.manageErrorWithRollback(err, "handleCompteUpdate : Impossible de mettre à jour le compte", w, r, http.StatusBadRequest)
			return
		}

		s.store.Commit()

		var resp = mapCompteToJson(compte)
		s.respond(w, r, resp, http.StatusOK)
	}
}

func mapCompteToJson(c *Compte) jsonCompte {
	return jsonCompte{
		Id:        c.Id,
		Libelle:   c.Libelle,
		Titulaire: c.Titulaire,
		Numero:    c.Numero,
		Actif:     c.Actif,
	}
}

func mapCompteToJsonComplete(c *CompteComplete) jsonCompteComplete {
	return jsonCompteComplete{
		Id:        c.Id,
		Libelle:   c.Libelle,
		Titulaire: c.Titulaire,
		Numero:    c.Numero,
		Actif:     c.Actif,
		Total:     c.Total,
		TotalReel: c.TotalReel,
	}
}
