package main

import "log"

func (store *dbStore) createOpTag(opTag *OpTag) error {

	query := "insert into optag (idOperation, idTag) values ($1, $2) RETURNING id"
	stmt, err := store.tx.Prepare(query)
	if err != nil {
		return err
	}
	defer stmt.Close()

	var id int64
	err = stmt.QueryRow(opTag.IdOperation, opTag.IdTag).Scan(&id)
	if err != nil {
		return err
	}

	opTag.Id = id
	return err
}

/*
------------------------------------------------------------------------------------
Suppression des optag pour une opération donnée
(renvoie le nombre de lignes supprimées)
------------------------------------------------------------------------------------
*/
func (store *dbStore) deleteOpTagByOperation(idOperation int64) (int64, error) {

	res, err := store.tx.Exec("delete from opTag where idOperation = $1", idOperation)
	if err != nil {
		log.Printf("deleteOpTagByOperation : erreur détectée au delete de l'optag err : %v", err)
		return 0, err
	}

	count, err := res.RowsAffected()

	if err != nil {
		log.Printf("deleteOpTagByOperation : la base de données ne retourne pas le nbre de ligne supprimée")
		return 0, nil
	}

	return count, nil
}

/*
------------------------------------------------------------------------------------
Suppression des optag pour une Tag donné
(renvoie le nombre de lignes supprimées)
------------------------------------------------------------------------------------
*/
func (store *dbStore) deleteOpTag4Tag(idTag int64) (int64, error) {
	res, err := store.db.Exec("delete from opTag where idTag = $1", idTag)
	if err != nil {
		log.Printf("deleteOpTag4Tag : erreur détectée au delete de l'optag err : %v", err)
		return 0, err
	}

	count, err := res.RowsAffected()

	if err != nil {
		log.Printf("deleteOpTag4Tag : la base de données ne retourne pas le nbre de ligne supprimée")
		return 0, nil
	}

	return count, nil
}

/*
-----------------------------------------------------------------------------------------
Suppression des opTags pour toutes les opértions
on passe l'id du tag à supprimer
Renvoie le nombre de lignes supprimées ou une erreur
-----------------------------------------------------------------------------------------
*/
func (store *dbStore) removeOpTagForOperations(inTag int64) (int64, error) {

	resultatSQL, err := store.db.Exec("delete optag where idTag = ?", inTag)

	if err != nil {
		return 0, err
	}

	count, err := resultatSQL.RowsAffected()
	if err != nil {
		log.Printf("la base de données ne retourne pas le nbre de ligne supprimée")
		return 0, nil
	}

	return count, nil
}
