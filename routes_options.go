package main

import (
	"log"
	"net/http"
)

func (s *server) handleOptions() http.HandlerFunc {

	return func(w http.ResponseWriter, r *http.Request) {

		s.enableCors(&w)
		log.Printf("Options : OK")
		s.respond(w, r, nil, http.StatusOK)
	}
}
