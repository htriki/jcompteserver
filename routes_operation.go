package main

import (
	"log"
	"net/http"
	"strconv"
	"time"

	"github.com/gorilla/mux"
)

type jsonOperation struct {
	Id             int64      `json:"id"`
	DateOperation  time.Time  `json:"dateOperation"`
	Libelle        string     `json:"libelle"`
	Description    string     `json:"description"`
	Debit          float32    `json:"debit"`
	Credit         float32    `json:"credit"`
	Valide         bool       `json:"valide"`
	Annuler        bool       `json:"annuler"`
	IdLink         int64      `json:"idLink"`
	IdCompteLinked int64      `json:"idCompteLinked"`
	Compte         jsonCompte `json:"compte"`
	Tags           []jsonTag  `json:"tags"`
	Moyen          jsonMoyen  `json:"moyen"`
}

type jsonSearchOperation struct {
	IdCompte int64  `json:"idCompte"`
	Value    string `json:"value"`
}

/*
------------------------------------------------------------------
Toutes les opérations
------------------------------------------------------------------
*/
func (s *server) handleOperation() http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {

		s.enableCors(&w)

		operations, err := s.store.findOperations()

		if err != nil {
			s.manageError(err, "Pb de récupération les operations", w, r, http.StatusInternalServerError)
			return
		}

		var resp = make([]jsonOperation, len(operations))
		for i, c := range operations {
			resp[i] = mapOperationToJson(c)
		}

		s.respond(w, r, resp, http.StatusOK)
	}
}

/*
------------------------------------------------------------------
Toutes les opérations por 1 compte donné
------------------------------------------------------------------
*/
func (s *server) handleOperationByCompte() http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {

		s.enableCors(&w)

		vars := mux.Vars(r)
		idCompte, err := strconv.ParseInt(vars["idCompte"], 10, 64)
		if err != nil {
			s.manageError(err, "handleOperationByCompte : Pb de parse du parametre", w, r, http.StatusInternalServerError)
			return
		}
		operations, err := s.store.findOperationByCompte(idCompte)

		if err != nil {
			s.manageError(err, "handleOperationByCompte : Pb de récupération les operations", w, r, http.StatusInternalServerError)
			return
		}

		var resp = make([]jsonOperation, len(operations))
		for i, c := range operations {
			resp[i] = mapOperationToJson(c)
		}

		s.respond(w, r, resp, http.StatusOK)

	}
}

/*
------------------------------------------------------------------
Toutes les opérations por 1 compte donné et une valeur de recherche
------------------------------------------------------------------
*/
func (s *server) handleOperationByCompteAndSearch() http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {

		s.enableCors(&w)

		req := jsonSearchOperation{}
		err := s.decode(w, r, &req)

		if err != nil {
			s.manageError(err, "handleOperationByCompteAndSearch : Impossible de parse les valeurs d'entrées", w, r, http.StatusBadRequest)
			return
		}

		operations, err := s.store.findOperationByCompteAndSearch(req.IdCompte, req.Value)

		if err != nil {
			s.manageError(err, "handleOperationByCompteAndSearch : Pb de récupération les operations", w, r, http.StatusInternalServerError)
			return
		}

		var resp = make([]jsonOperation, len(operations))
		for i, c := range operations {
			resp[i] = mapOperationToJson(c)
		}

		s.respond(w, r, resp, http.StatusOK)
	}
}

/*
------------------------------------------------------------------
Nombre total d'opération pour un compte donné
------------------------------------------------------------------
*/
func (s *server) handleCountOperationByCompte() http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {

		s.enableCors(&w)
		vars := mux.Vars(r)
		idCompte, err := strconv.ParseInt(vars["idCompte"], 10, 64)

		if err != nil {
			s.manageError(err, "handleCountOperationByCompte : Pb de parse du parametre", w, r, http.StatusBadRequest)
			return
		}

		count, err := s.store.countOperationByCompte(idCompte)

		if err != nil {
			s.manageError(err, "handleCountOperationByCompte : Pb de récupération du nombre d'opération pour un compte donné", w, r, http.StatusInternalServerError)
			return
		}

		s.respond(w, r, count, http.StatusOK)
	}
}

/*
------------------------------------------------------------------
Detail d'une operation
------------------------------------------------------------------
*/
func (s *server) handleOperationDetail() http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {

		s.enableCors(&w)

		vars := mux.Vars(r)
		id, err := strconv.ParseInt(vars["id"], 10, 64)

		if err != nil {
			s.manageError(err, "handleOperationDetail : Pb de parse du parametre", w, r, http.StatusBadRequest)
			return
		}

		operation, err := s.store.findOperationById(id)

		if err != nil {
			s.manageError(err, "handleOperationDetail : Operation avec probleme", w, r, http.StatusInternalServerError)
			return
		}

		var resp = mapOperationToJson(&operation)

		s.respond(w, r, resp, http.StatusOK)
	}
}

/*
------------------------------------------------------------------
Création d'une opération
------------------------------------------------------------------
*/
func (s *server) handleOperationCreate() http.HandlerFunc {

	return func(w http.ResponseWriter, r *http.Request) {

		s.enableCors(&w)

		req := jsonOperation{}
		err := s.decode(w, r, &req)

		if err != nil {
			s.manageError(err, "handleOperationCreate : Impossible de parse l'operation", w, r, http.StatusBadRequest)
			return
		}

		// Création d'une opération
		op1 := &Operation{
			Id:            0,
			DateOperation: req.DateOperation,
			Libelle:       req.Libelle,
			Description:   req.Description,
			Debit:         req.Debit,
			Credit:        req.Credit,
			Valide:        req.Valide,
			Annuler:       req.Annuler,
			IdLink:        req.IdLink,
			Compte:        Compte(req.Compte),
			Moyen:         Moyen(req.Moyen),
			//Tags:          tags,
		}

		// Démarrage d'une transaction
		s.store.BeginTx()

		// Sauvegarde de l'opération 1
		err = s.store.createOperation(op1)

		if err != nil {
			s.manageErrorWithRollback(err, "handleOperationCreate : Impossible de créer l'opération", w, r, http.StatusBadRequest)
			return
		}

		// Récupération des Tags et création des enregistrement dans OpTag
		lenTags := len(req.Tags)

		if lenTags > 0 {

			for _, jtag := range req.Tags {
				t := OpTag{0, op1.Id, jtag.Id}
				err := s.store.createOpTag(&t)

				if err != nil {
					s.manageErrorWithRollback(err, "handleOperationCreate : Impossible de créer les Tag", w, r, http.StatusBadRequest)
					return
				}
			}
		}

		if req.IdCompteLinked != 0 {

			// Gestion de la liaison d'opération
			// Dans ce cas on n'ajoute pas les tags dans l'opération liée (si toutefois il y en avait)

			// recherche du compte lié à partir de son Id
			compteLinked, err := s.store.findCompteById(req.IdCompteLinked)

			if err != nil {
				s.manageErrorWithRollback(err, "handleOperationCreate : Impossible de trouver le compte lié", w, r, http.StatusBadRequest)
				return
			}

			op2 := &Operation{
				Id:            0,
				DateOperation: req.DateOperation,
				Libelle:       req.Libelle,
				Description:   req.Description,
				Debit:         req.Credit,
				Credit:        req.Debit,
				Valide:        req.Valide,
				Annuler:       req.Annuler,
				IdLink:        op1.Id,
				Compte:        *compteLinked,
				Moyen:         Moyen(req.Moyen),
				//Tags:        // Pas de Tags pour l'opération 2
			}

			// Sauvegarde de l'opération 2
			err = s.store.createOperation(op2)

			if err != nil {
				s.manageErrorWithRollback(err, "handleOperationCreate : Impossible de créer l'opération liée", w, r, http.StatusInternalServerError)
				return
			}

			// Mise à jour de l'opération 1 avec l'id de l'opération 2
			op1.IdLink = op2.Id

			err = s.store.updateOperation(op1)
			if err != nil {
				s.manageErrorWithRollback(err, "handleOperationCreate : Impossible de mettre à jour l'opération", w, r, http.StatusInternalServerError)
				return
			}
		}

		s.store.Commit()

		var resp = mapOperationToJson(op1)
		s.respond(w, r, resp, http.StatusOK)
	}
}

/*
------------------------------------------------------------------
Mise à jour d'une opération
------------------------------------------------------------------
*/
func (s *server) handleOperationUpdate() http.HandlerFunc {

	return func(w http.ResponseWriter, r *http.Request) {

		s.enableCors(&w)

		req := jsonOperation{}
		err := s.decode(w, r, &req)

		if err != nil {
			s.manageError(err, "handleOperationUpdate : Impossible de parser l'opération", w, r, http.StatusBadRequest)
			return
		}

		// Opération
		op := &Operation{
			Id:            req.Id,
			DateOperation: req.DateOperation,
			Libelle:       req.Libelle,
			Description:   req.Description,
			Debit:         req.Debit,
			Credit:        req.Credit,
			Valide:        req.Valide,
			Annuler:       req.Annuler,
			IdLink:        req.IdLink,
			Compte:        Compte(req.Compte),
			Moyen:         Moyen(req.Moyen),
			//Tags:          tags,
		}

		// Démarrage de la transaction
		s.store.BeginTx()

		// Mise à jour de l'opération
		err = s.store.updateOperation(op)

		if err != nil {
			s.manageErrorWithRollback(err, "handleOperationUpdate : Impossible de mettre à jour l'opération", w, r, http.StatusInternalServerError)
			return
		}

		// Suppression des opTag existants pour les remettre à jour après
		_, err = s.store.deleteOpTagByOperation(op.Id)

		if err != nil {
			s.manageErrorWithRollback(err, "handleOperationUpdate : Impossible de supprimer les tags existant pendant la mise à jour de l'opération", w, r, http.StatusInternalServerError)
			return
		}

		// Récupération des Tags et création des enregistrement dans OpTag
		lenTags := len(req.Tags)

		if lenTags > 0 {

			for _, jtag := range req.Tags {
				t := OpTag{0, op.Id, jtag.Id}
				err := s.store.createOpTag(&t)

				if err != nil {
					s.manageErrorWithRollback(err, "handleOperationUpdate : Impossible de recréer les Tags (update d'une operation)", w, r, http.StatusInternalServerError)
					return
				}
			}
		}

		// s'il existe une opération liée, mettre aussi à jour l'opération liée sans les Tags
		if op.IdLink != 0 {

			opL, err := s.store.findOperationById(op.IdLink)

			if err != nil {
				s.manageErrorWithRollback(err, "handleOperationUpdate : Impossible de récupérer l'opération liée", w, r, http.StatusInternalServerError)
				return
			}

			opL.DateOperation = req.DateOperation
			opL.Libelle = req.Libelle
			opL.Description = req.Description
			opL.Debit = req.Credit
			opL.Credit = req.Debit
			opL.Valide = req.Valide
			opL.Annuler = req.Annuler
			opL.Moyen = Moyen(req.Moyen)

			err = s.store.updateOperation(&opL)

			if err != nil {
				s.manageErrorWithRollback(err, "handleOperationUpdate : Impossible de mettre à jour l'opération liée", w, r, http.StatusInternalServerError)
				return
			}
		}

		s.store.Commit()

		var resp = mapOperationToJson(op)
		s.respond(w, r, resp, http.StatusOK)
	}
}

/*
------------------------------------------------------------------
Suppression d'une opération
------------------------------------------------------------------
*/
func (s *server) handleOperationDelete() http.HandlerFunc {

	return func(w http.ResponseWriter, r *http.Request) {

		s.enableCors(&w)

		vars := mux.Vars(r)
		idOperation, err := strconv.ParseInt(vars["id"], 10, 64)

		if err != nil {
			s.manageError(err, "handleOperationDelete : Pb de parse du parametre", w, r, http.StatusInternalServerError)
			return
		}

		// recherche de l'opération pour vérifier s'il existe une opération liée
		operationToDelete, err := s.store.findOperationById(idOperation)

		if err != nil {
			s.manageError(err, "handleOperationDelete : Pb pour trouver l'opération à partir de son id", w, r, http.StatusInternalServerError)
			return
		}

		// Début d'une transaction
		s.store.BeginTx()

		// Suppression d'abord l'opération liée
		if operationToDelete.IdLink != 0 {

			_, err := s.store.deleteOpTagByOperation(operationToDelete.IdLink)

			if err != nil {
				s.manageErrorWithRollback(err, "handleOperationDelete : Erreur lors de la suppression des optag pour l'opération liée", w, r, http.StatusInternalServerError)
				return
			}

			_, err = s.store.deleteOperation(operationToDelete.IdLink)

			if err != nil {
				s.manageErrorWithRollback(err, "handleOperationDelete : Erreur lors de la suppression de l'operation liée", w, r, http.StatusInternalServerError)
				return
			}
		}

		// Suppression des Tags de l'opération
		count, err := s.store.deleteOpTagByOperation(idOperation)

		if err != nil {
			s.manageErrorWithRollback(err, "handleOperationDelete : Erreur lors de la suppression des optag pour l'opération", w, r, http.StatusInternalServerError)
			return
		}

		log.Printf("nombre de ligne supprimée de la table opTag  %v", count)

		// Suppression de l'opération
		count, err = s.store.deleteOperation(idOperation)

		if err != nil {
			s.manageErrorWithRollback(err, "handleOperationDelete : Erreur lors de la suppression de l'operation", w, r, http.StatusInternalServerError)
			return
		}

		log.Printf("nombre de ligne supprimée de la table operation %v", count)
		s.store.Commit()

		s.respond(w, r, count, http.StatusOK)
	}
}

/*
------------------------------------------------------------------
Validation d'une opération
------------------------------------------------------------------
*/
func (s *server) handleOperationValide(valide bool) http.HandlerFunc {

	type jsonRequest struct {
		List []int64 `json:"list"`
	}

	return func(w http.ResponseWriter, r *http.Request) {

		s.enableCors(&w)

		req := jsonRequest{}

		err := s.decode(w, r, &req)

		if err != nil {
			s.manageErrorWithRollback(err, "handleOperationValide : Impossible de parse la list des operations", w, r, http.StatusBadRequest)
			return
		}

		s.store.BeginTx()

		err = s.store.valideOperations(req.List, valide)

		if err != nil {
			s.manageErrorWithRollback(err, "handleOperationValide : Impossible de valider la liste d'opératione", w, r, http.StatusBadRequest)
			return
		}

		s.store.Commit()

		s.respond(w, r, nil, http.StatusOK)
	}
}

/*
------------------------------------------------------------------
Annulation d'une opération
------------------------------------------------------------------
*/
func (s *server) handleOperationCancel(annuler bool) http.HandlerFunc {

	type jsonRequest struct {
		List []int64 `json:"list"`
	}

	return func(w http.ResponseWriter, r *http.Request) {

		s.enableCors(&w)

		req := jsonRequest{}

		err := s.decode(w, r, &req)

		if err != nil {
			s.manageError(err, "handleOperationCancel : Impossible de parse la list des operations", w, r, http.StatusBadRequest)
			return
		}

		s.store.BeginTx()

		// Sauvegarde de l'opération
		err = s.store.annulerOperations(req.List, annuler)

		if err != nil {
			s.manageErrorWithRollback(err, "handleOperationCancel : Impossible d'annuler la liste d'opération", w, r, http.StatusInternalServerError)
			return
		}

		s.store.Commit()

		s.respond(w, r, nil, http.StatusOK)
	}
}

func (s *server) handleOperationDupliquer() http.HandlerFunc {

	type jsonRequest struct {
		DateDupliquer time.Time `json:"dateDupliquer"`
		List          []int64   `json:"list"`
	}

	return func(w http.ResponseWriter, r *http.Request) {

		s.enableCors(&w)

		req := jsonRequest{}

		err := s.decode(w, r, &req)

		if err != nil {
			s.manageError(err, "handleOperationDupliquer : Impossible de parse la list des operations à dupliquer", w, r, http.StatusBadRequest)
			return
		}

		s.store.BeginTx()

		// Duplication des opérations une par une
		for _, id := range req.List {

			operation, err := s.store.findOperationById(id)

			if err != nil {
				s.manageErrorWithRollback(err, "handleOperationDupliquer : Erreur qui s'est produite sur la duplication d'une opération", w, r, http.StatusInternalServerError)
				return
			}

			operation.DateOperation = req.DateDupliquer

			err = s.store.createOperation(&operation)
			if err != nil {
				s.manageErrorWithRollback(err, "handleOperationDupliquer : Erreur createOperation", w, r, http.StatusInternalServerError)
				return
			}

			lenTags := len(operation.Tags)

			if lenTags > 0 {
				for _, tag := range operation.Tags {
					t := OpTag{0, operation.Id, tag.Id}
					err = s.store.createOpTag(&t)

					if err != nil {
						s.manageErrorWithRollback(err, "handleOperationDupliquer : Impossible de créer les Tag", w, r, http.StatusInternalServerError)
						return
					}
				}
			}

		}

		s.store.Commit()
		s.respond(w, r, nil, http.StatusOK)
	}
}

/*
------------------------------------------------------------------
Recherche d'un libelle dans une opération (de n'importe quel compte, on s'en fou)
------------------------------------------------------------------
*/
func (s *server) handleOperationSearchLibelle() http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {

		s.enableCors(&w)

		vars := mux.Vars(r)
		search := vars["search"]

		tab, err := s.store.findLibelleBySearch(search)

		if err != nil {
			s.manageError(err, "handleOperationSearchLibelle : Libelle inconnue", w, r, http.StatusInternalServerError)
			return
		}

		s.respond(w, r, tab, http.StatusOK)
	}
}

func (s *server) handleOperationLibelles() http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {

		s.enableCors(&w)

		tab, err := s.store.findLibelles()
		if err != nil {
			s.manageError(err, "handleOperationLibelles : Probleme pour récupérer tous les libelles", w, r, http.StatusInternalServerError)
			return
		}

		s.respond(w, r, tab, http.StatusOK)
	}
}

/*
------------------------------------------------------------------
Conversion d'une opération en JSON
------------------------------------------------------------------
*/
func mapOperationToJson(op *Operation) jsonOperation {

	tags := make([]jsonTag, 0)
	for _, t := range op.Tags {
		tags = append(tags, mapTagToJson(&t))
	}

	jsonOp := jsonOperation{
		Id:            op.Id,
		DateOperation: op.DateOperation,
		Libelle:       op.Libelle,
		Description:   op.Description,
		Debit:         op.Debit,
		Credit:        op.Credit,
		Valide:        op.Valide,
		Annuler:       op.Annuler,
		IdLink:        op.IdLink,
		Compte:        jsonCompte(op.Compte),
		Tags:          tags,
		Moyen:         jsonMoyen(op.Moyen),
	}
	return jsonOp
}
