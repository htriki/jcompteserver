# PROJET jcompteserver
author  : Hocine 2023  
langage : Golang  
  
### List des dépendances (~/go/pkg/mod)  
### go list -m all  

github.com/dgrijalva/jwt-go v3.2.0+incompatible  
github.com/go-sql-driver/mysql v1.6.0  
github.com/gorilla/mux v1.8.0  
github.com/jmoiron/sqlx v1.3.5  
github.com/lib/pq v1.10.9  
github.com/mattn/go-sqlite3 v1.14.6  

### run
local   :   go run . -env local  
prod    :   ./jcompteserver -env prod  

### build  
local   :   go build .
prod    :   GOARCH=arm GOOS=linux go build -o jcompteserver

### service
restart :   sudo systemctl daemon-reload
            sudo systemctl restart jcompteserver
            sudo systemctl enable jcompteserver
journal :   sudo journalctl -u jcompteserver --since "15 minutes ago"



