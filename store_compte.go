package main

import "log"

func (store *dbStore) findCompte() ([]*CompteComplete, error) {

	var comptes []*CompteComplete
	sql := `select c.* , 
			(select coalesce(sum(credit) - sum(debit), 0) from operation where idCompte = c.id and annuler=false) as total,
			(select coalesce(sum(credit) - sum(debit), 0) from operation where idCompte = c.id and valide=true and annuler=false) as totalreel
			from compte c
			order by libelle, titulaire`

	err := store.db.Select(&comptes, sql)
	if err != nil {
		return comptes, err
	}

	return comptes, nil
}

func (store *dbStore) findCompteActif() ([]*Compte, error) {

	var comptes []*Compte
	sql := `select * from compte c 
			where actif = true
			order by libelle, titulaire`

	err := store.db.Select(&comptes, sql)
	if err != nil {
		return comptes, err
	}

	return comptes, nil
}

func (store *dbStore) findCompteById(id int64) (*Compte, error) {

	var cpte = &Compte{}
	err := store.db.Get(cpte, "select * from compte where id = $1", id)
	if err != nil {
		return cpte, err
	}

	return cpte, nil
}

func (store *dbStore) deleteCompte(id int64) (int64, error) {

	res, err := store.tx.Exec("delete from compte where id = $1", id)
	if err != nil {
		return 0, err
	}

	count, err := res.RowsAffected()

	if err != nil {
		log.Printf("deleteCompte : la base de données ne retourne pas le nbre de ligne supprimée")
		return 0, nil
	}

	return count, nil
}

func (store *dbStore) createCompte(compte *Compte) error {

	query := "Insert into compte (libelle, titulaire, numero, actif) values ($1, $2, $3, $4) RETURNING id"
	stmt, err := store.tx.Prepare(query)
	if err != nil {
		return err
	}
	defer stmt.Close()

	var id int64
	err = stmt.QueryRow(compte.Libelle, compte.Titulaire, compte.Numero, compte.Actif).Scan(&id)
	if err != nil {
		return err
	}

	compte.Id = id
	return err
}

func (store *dbStore) updateCompte(compte *Compte) error {

	sql := "update compte set libelle = $1, titulaire = $2, numero = $3, actif = $4 where id = $5"

	_, err := store.tx.Exec(
		sql,
		compte.Libelle, compte.Titulaire, compte.Numero, compte.Actif, compte.Id,
	)
	if err != nil {
		return err
	}
	return err
}
