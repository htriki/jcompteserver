package main

import "log"

func (store *dbStore) findTag() ([]*Tag, error) {

	var tags []*Tag
	err := store.db.Select(&tags, "select * from tag order by libelle")
	if err != nil {
		return tags, err
	}

	return tags, nil
}

func (store *dbStore) createTag(tag *Tag) error {

	query := "Insert into tag (libelle, couleur) values ($1, $2) RETURNING id"
	stmt, err := store.db.Prepare(query)
	if err != nil {
		return err
	}
	defer stmt.Close()

	var id int64
	err = stmt.QueryRow(tag.Libelle, tag.Couleur).Scan(&id)
	if err != nil {
		return err
	}

	tag.Id = id
	return err
}

func (store *dbStore) updateTag(tag *Tag) error {

	sql := "update tag set libelle = $1, couleur = $2 where id = $3"

	_, err := store.db.Exec(sql, tag.Libelle, tag.Couleur, tag.Id)
	if err != nil {
		return err
	}
	return err
}

func (store *dbStore) deleteTag(idTag int64) (int64, error) {

	res, err := store.db.Exec("delete from tag where id = $1", idTag)
	if err != nil {
		return 0, err
	}

	count, err := res.RowsAffected()

	if err != nil {
		log.Printf("deleteTag : la base de données ne retourne pas le nbre de ligne supprimée")
		return 0, nil
	}

	return count, nil
}
