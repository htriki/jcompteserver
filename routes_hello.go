package main

import (
	"net/http"
	"time"
)

func (s *server) handleHello() http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {

		s.enableCors(&w)
		t := time.Now()
		s.respond(w, r, "Hello : "+t.String(), http.StatusOK)

	}
}
