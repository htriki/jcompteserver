package main

import (
	"fmt"
	"net/http"
	"time"

	"github.com/dgrijalva/jwt-go"
)

// fonction associé à server qui renvoir une function de type HandlerFunc
func (s *server) handleIndex() http.HandlerFunc {
	return func (w http.ResponseWriter, r *http.Request) {
		fmt.Fprintf(w, "Welcome !!!")
	}
}


func (s *server) handleTokenCreate() http.HandlerFunc {
	type request struct {
		Username string `json:"username"`
		Password string `json:"password"`
	}

	type response struct {
		Token string `json: "token"`
	}

	type responseError struct {
		Error string `json: "error"`
	}

	return func (w http.ResponseWriter, r *http.Request) {
		
		req := request{}
		err := s.decode(w, r, &req)
		if err != nil {
			msg := fmt.Sprintf("Impossible de parser. err=%v", err)
			s.respond(w, r,responseError{
				Error: msg,
			}, http.StatusBadRequest)
			return
		}

		if req.Username != "test" || req.Password != "test" {
			s.respond(w, r, responseError{
				Error: "Invalid credientials",
			}, http.StatusUnauthorized)
			return
		}

		// Generation du token
		token := jwt.NewWithClaims(jwt.SigningMethodES256, jwt.MapClaims{
			"username": req.Username,
			"exp": time.Now().Add(time.Hour * time.Duration(1)).Unix(),
			"iat": time.Now().Unix(),
		})

		tokenStr, err := token.SignedString([]byte(JWT_APP_KEY))
		if err != nil {
			msg := fmt.Sprintf("Impossible de générer le JWT. err=%v", err)
			s.respond(w, r, responseError{
				Error: msg,
			}, http.StatusInternalServerError)
			return
		}

		// Envoi du token
		s.respond(w, r, response{
			Token: tokenStr,
		}, http.StatusOK)
	} 
}
