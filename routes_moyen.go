package main

import (
	"log"
	"net/http"
	"strconv"

	"github.com/gorilla/mux"
)

type jsonMoyen struct {
	Id      int64  `json:"id"`
	Libelle string `json:"libelle"`
}

func (s *server) handleMoyen() http.HandlerFunc {

	return func(w http.ResponseWriter, r *http.Request) {

		s.enableCors(&w)

		moyens, err := s.store.findMoyen()
		if err != nil {
			log.Printf("Pb de récupération des myens. err=%v\n", err)
			s.respond(w, r, nil, http.StatusInternalServerError)
			return
		}

		var resp = make([]jsonMoyen, len(moyens))
		for i, m := range moyens {
			resp[i] = mapMoyenToJson(m)
		}

		s.respond(w, r, resp, http.StatusOK)

	}
}

func (s *server) handleMoyenCreate() http.HandlerFunc {

	type request struct {
		Libelle string `json:"libelle"`
	}

	return func(w http.ResponseWriter, r *http.Request) {

		s.enableCors(&w)

		req := request{}
		err := s.decode(w, r, &req)
		if err != nil {
			log.Printf("Impossible de parse le moyen %v", err)
			s.respond(w, r, nil, http.StatusBadRequest)
			return
		}

		// Créer un moyen
		moyen := &Moyen{
			Id:      0,
			Libelle: req.Libelle,
		}

		// Sauvegarde du moyen
		err = s.store.createMoyen(moyen)
		if err != nil {
			log.Printf("Impossible de créer le moyen err=%v", err)
			s.respond(w, r, nil, http.StatusInternalServerError)
			return
		}

		var resp = mapMoyenToJson(moyen)
		s.respond(w, r, resp, http.StatusOK)
	}
}

func (s *server) handleMoyenUpdate() http.HandlerFunc {

	return func(w http.ResponseWriter, r *http.Request) {

		s.enableCors(&w)

		req := jsonMoyen{}
		err := s.decode(w, r, &req)
		if err != nil {
			log.Printf("Impossible de parse le moyen %v", err)
			s.respond(w, r, nil, http.StatusBadRequest)
			return
		}
		moyen := &Moyen{
			Id:      req.Id,
			Libelle: req.Libelle,
		}

		// Mise à jour du moyen
		err = s.store.updateMoyen(moyen)
		if err != nil {
			log.Printf("Impossible de mettre à jour le moyen err=%v", err)
			s.respond(w, r, nil, http.StatusInternalServerError)
			return
		}

		var resp = mapMoyenToJson(moyen)
		s.respond(w, r, resp, http.StatusOK)
	}
}

/*
---------------------------------------------------------------------------------------
Avant la suppression d'un moyen supprimer les moyens dans les opérations liées
---------------------------------------------------------------------------------------
*/
func (s *server) handleMoyenDelete() http.HandlerFunc {

	return func(w http.ResponseWriter, r *http.Request) {

		s.enableCors(&w)

		vars := mux.Vars(r)
		idMoyen, err := strconv.ParseInt(vars["id"], 10, 64) // 10 pour décimal et 64 car c'st un int64
		if err != nil {
			log.Printf("Pb de parse du parametre")
			s.respond(w, r, nil, http.StatusBadRequest)
			return
		}

		err = s.store.deleteMoyenInOperation(idMoyen) // id = id du moyen
		if err != nil {
			log.Printf("Erreur lors de la suppression des moyens dans la table Opération : %v. err=%v\n", idMoyen, err)
			s.respond(w, r, nil, http.StatusInternalServerError)
			return
		}

		count, err := s.store.deleteMoyen(idMoyen)
		if err != nil {
			log.Printf("Erreur lors de la suppression du moyen : %v. err=%v\n", idMoyen, err)
			s.respond(w, r, nil, http.StatusInternalServerError)
			return
		}

		log.Printf("nombre de ligne supprimée : %v", count)

		s.respond(w, r, count, http.StatusOK)
	}
}

func mapMoyenToJson(c *Moyen) jsonMoyen {
	return jsonMoyen{
		Id:      c.Id,
		Libelle: c.Libelle,
	}
}
