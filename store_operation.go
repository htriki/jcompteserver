package main

import (
	"log"
	"strconv"
)

const SQL_OPERATION = `select o.id as idOperation, o.dateOperation,  o.libelle as libelleOperation, o.description, 
						coalesce(o.debit, 0) as debit, 
						coalesce(o.credit, 0) as credit, 
						o.valide, o.annuler, o.idCompte, 
						c.libelle as libelleCompte,
						c.titulaire as titulaireCompte,
						c.numero as numeroCompte,
						c.actif as actifCompte,
						t.id as idTag,
						t.libelle as libelleTag,
						t.couleur as couleurTag,
						o.idMoyen,
						m.libelle as libelleMoyen,
						o.idlink
						from operation o 
						inner join compte c on o.idCompte = c.id
						left join optag ot on o.id = ot.idOperation
						left join tag t on t.id = ot.idTag
						left join moyen m on m.id = o.idMoyen `

func (store *dbStore) findOperations() ([]*Operation, error) {

	var operations []*Operation

	sql := SQL_OPERATION + " order by o.id"

	rows, err := store.db.Query(sql)

	if err != nil {
		log.Printf("Erreur recuperation des operations : %v", err)
		return operations, err
	}

	defer rows.Close()

	var op *Operation
	var idCopie int64
	idCopie = 0

	for rows.Next() {

		var r OperationRecord
		err := rows.Scan(&r.IdOperation, &r.DateOperation, &r.LibelleOperation, &r.Description, &r.Debit, &r.Credit, &r.Valide, &r.Annuler,
			&r.IdCompte, &r.LibelleCompte, &r.TitulaireCompte, &r.NumeroCompte, &r.ActifCompte,
			&r.IdTag, &r.LibelleTag, &r.CouleurTag, &r.IdMoyen, &r.LibelleMoyen, &r.IdLink)
		if err != nil {
			log.Printf("findOperations : erreur au scan des enregistements %v", err)
			return nil, err
		}

		if r.IdOperation != idCopie {

			idCopie = r.IdOperation
			op = &Operation{}

			affectation(op, r)
			operations = append(operations, op)
		}

		if r.IdTag.Valid {

			tag := Tag{r.IdTag.Int64, r.LibelleTag.String, r.CouleurTag.String}
			op.Tags = append(op.Tags, tag)
		}
	}

	return operations, nil
}

/*
--------------------------------------------------------------------------------------
Renvoie toutes les opérations pour 1 compte donné
--------------------------------------------------------------------------------------
*/
func (store *dbStore) findOperationByCompte(idCompte int64) ([]*Operation, error) {

	var operations []*Operation

	sql := SQL_OPERATION + " where c.id = $1 order by o.dateOperation, o.id"

	rows, err := store.db.Query(sql, idCompte)

	if err != nil {
		log.Printf("findOperationByCompte : Erreur recuperation des operations : %v", err)
		return operations, err
	}

	defer rows.Close()

	var op *Operation
	var idCopie int64
	idCopie = 0

	for rows.Next() {

		var r OperationRecord
		err := rows.Scan(&r.IdOperation, &r.DateOperation, &r.LibelleOperation, &r.Description, &r.Debit, &r.Credit, &r.Valide, &r.Annuler,
			&r.IdCompte, &r.LibelleCompte, &r.TitulaireCompte, &r.NumeroCompte, &r.ActifCompte,
			&r.IdTag, &r.LibelleTag, &r.CouleurTag, &r.IdMoyen, &r.LibelleMoyen, &r.IdLink)
		if err != nil {
			log.Printf("erreur au scan des enregistements %v", err)
			return nil, err
		}

		if r.IdOperation != idCopie {

			idCopie = r.IdOperation
			op = &Operation{}

			affectation(op, r)
			operations = append(operations, op)
		}

		if r.IdTag.Valid {

			tag := Tag{r.IdTag.Int64, r.LibelleTag.String, r.CouleurTag.String}
			op.Tags = append(op.Tags, tag)
		}
	}

	return operations, nil
}

func (store *dbStore) countOperationByCompte(idCompte int64) (int, error) {

	var countOperation int

	//log.Printf("Compte operation : %v", idCompte)
	//sql := `select count(*) as countOperation from operation where idCompte = ?`
	//rows, err := store.db.Query(sql, idCompte)

	sql := "select count(*) from operation where idCompte = " + strconv.FormatInt(idCompte, 10)

	err := store.db.QueryRow(sql).Scan(&countOperation)

	if err != nil {
		log.Printf("Erreur recuperation du nombre d'opération par compte %v : %v", idCompte, err)
		return countOperation, err
	}

	return countOperation, nil
}

/*
--------------------------------------------------------------------------------------
Renvoie toutes les opérations pour 1 compte donné
Et une valeur de recherche
--------------------------------------------------------------------------------------
*/
func (store *dbStore) findOperationByCompteAndSearch(idCompte int64, value string) ([]*Operation, error) {

	var operations []*Operation

	sql := SQL_OPERATION + ` 
						where c.id = $1 
						and ( 
							o.libelle ilike $2
							or o.description ilike $2
							or t.libelle ilike $2
							or o.id in (
								select x.idOperation from optag x
								inner join tag y on x.idTag = y.id 
								where y.libelle like $2	
							)
							or m.libelle ilike $2
						)
						order by o.dateOperation, o.id`

	/*log.Printf("SQL %v", sql)
	log.Printf("idCompte : %v", idCompte)
	log.Printf("value : %v", value)*/

	rows, err := store.db.Query(sql, idCompte, "%"+value+"%")

	if err != nil {
		log.Printf("findOperationByCompte : Erreur recuperation des operations : %v", err)
		return operations, err
	}

	defer rows.Close()

	var op *Operation
	var idCopie int64
	idCopie = 0

	for rows.Next() {

		var r OperationRecord
		err := rows.Scan(&r.IdOperation, &r.DateOperation, &r.LibelleOperation, &r.Description, &r.Debit, &r.Credit, &r.Valide, &r.Annuler,
			&r.IdCompte, &r.LibelleCompte, &r.TitulaireCompte, &r.NumeroCompte, &r.ActifCompte,
			&r.IdTag, &r.LibelleTag, &r.CouleurTag, &r.IdMoyen, &r.LibelleMoyen, &r.IdLink)
		if err != nil {
			log.Printf("erreur au scan des enregistements %v", err)
			return nil, err
		}

		if r.IdOperation != idCopie {

			idCopie = r.IdOperation
			op = &Operation{}

			affectation(op, r)
			operations = append(operations, op)
		}

		if r.IdTag.Valid {

			tag := Tag{r.IdTag.Int64, r.LibelleTag.String, r.CouleurTag.String}
			op.Tags = append(op.Tags, tag)
		}
	}

	return operations, nil
}

/*
--------------------------------------------------------------------------------------
--------------------------------------------------------------------------------------
*/
func (store *dbStore) findOperationById(id int64) (Operation, error) {

	var operation Operation

	sql := SQL_OPERATION + " where o.id = $1"

	rows, err := store.db.Query(sql, id)

	if err != nil {
		log.Printf("Erreur recuperation de l'operation %v : %v", id, err)
		return operation, err
	}

	defer rows.Close()

	for rows.Next() {

		var r OperationRecord
		err := rows.Scan(&r.IdOperation, &r.DateOperation, &r.LibelleOperation, &r.Description, &r.Debit, &r.Credit, &r.Valide, &r.Annuler,
			&r.IdCompte, &r.LibelleCompte, &r.TitulaireCompte, &r.NumeroCompte, &r.ActifCompte,
			&r.IdTag, &r.LibelleTag, &r.CouleurTag, &r.IdMoyen, &r.LibelleMoyen, &r.IdLink)
		if err != nil {
			log.Printf("findOperationById : erreur au scan des enregistements %v", err)
			return operation, err
		}

		affectation(&operation, r)
		if r.IdTag.Valid {

			tag := Tag{r.IdTag.Int64, r.LibelleTag.String, r.CouleurTag.String}
			operation.Tags = append(operation.Tags, tag)
		}
	}

	return operation, nil
}

func (store *dbStore) findLibelles() ([]string, error) {

	var tab []string

	sql := "select distinct libelle from operation order by libelle"

	rows, err := store.db.Query(sql)

	if err != nil {
		log.Printf("Erreur de la recherche de tous les libellés %v", err)
		return nil, err
	}

	defer rows.Close()

	for rows.Next() {

		var s string
		err := rows.Scan(&s)
		if err != nil {
			log.Printf("findLibelles : erreur au scan du libelle %v", err)
			return tab, err
		}

		tab = append(tab, s)
	}

	return tab, nil
}

/*
------------------------------------------------------------------------------------------------------
Recherche de libelle
------------------------------------------------------------------------------------------------------
*/
func (store *dbStore) findLibelleBySearch(value string) ([]string, error) {

	var tab []string

	sql := "select distinct libelle from operation where libelle like concat('%', $1::text, '%')"

	rows, err := store.db.Query(sql, value)

	if err != nil {
		log.Printf("Erreur de la recherche de libelle %v : %v", "d%", err)
		return nil, err
	}

	defer rows.Close()

	for rows.Next() {

		var s string
		err := rows.Scan(&s)
		if err != nil {
			log.Printf("findLibelleBySearch : erreur au scan du libelle %v", err)
			return tab, err
		}

		tab = append(tab, s)
	}

	return tab, nil
}

/*
------------------------------------------------------------------------------------------------------
Copie des données OperationRecord vers Operation
------------------------------------------------------------------------------------------------------
*/
func affectation(op *Operation, r OperationRecord) {

	op.Id = r.IdOperation
	op.DateOperation = r.DateOperation
	if r.LibelleOperation.Valid {
		op.Libelle = r.LibelleOperation.String
	}
	if r.Description.Valid {
		op.Description = r.Description.String
	}
	op.Debit = r.Debit
	op.Credit = r.Credit
	op.Valide = r.Valide
	op.Annuler = r.Annuler
	op.Compte = Compte{r.IdCompte, r.LibelleCompte, r.TitulaireCompte, r.NumeroCompte, r.ActifCompte}
	if r.IdMoyen.Valid {
		op.Moyen = Moyen{r.IdMoyen.Int64, r.LibelleMoyen.String}
	}
	if r.IdLink.Valid {
		op.IdLink = r.IdLink.Int64
	}
}

func (store *dbStore) createOperation(operation *Operation) error {

	strIdMoyen := "null"
	if operation.Moyen.Id != 0 {
		strIdMoyen = strconv.FormatInt(operation.Moyen.Id, 10)
	}

	query := "insert into operation (dateOperation, libelle, description, debit, credit, valide, annuler, idCompte, idMoyen, idLink)" +
		" values ($1, $2, $3, $4, $5, $6, $7, $8, " +
		strIdMoyen +
		", $9) RETURNING id"

	stmt, err := store.tx.Prepare(query)
	if err != nil {
		return err
	}
	defer stmt.Close()

	var id int64
	err = stmt.QueryRow(operation.DateOperation, operation.Libelle, operation.Description, operation.Debit, operation.Credit, operation.Valide, operation.Annuler, operation.Compte.Id, operation.IdLink).Scan(&id)
	if err != nil {
		return err
	}

	operation.Id = id
	return err
}

/*
func (store *dbStore) dupliquerOperation(idFrom int64, dateOperation time.Time) error {

	operation, err := s.findOperationById(idFrom)

	_, err := store.db.Exec(sql, annuler)

	if err != nil {
		return err
	}
	return nil
} */

func (store *dbStore) deleteOperation(id int64) (int64, error) {

	res, err := store.tx.Exec("delete from operation where id = $1", id)
	if err != nil {
		return 0, err
	}

	count, err := res.RowsAffected()

	if err != nil {
		log.Printf("deleteOperation : la base de données ne retourne pas le nbre de ligne supprimée")
		return 0, nil
	}

	return count, nil
}

/*
Met à jour le champ valide de la liste des opérations passées en parametre
*/
func (store *dbStore) valideOperations(list []int64, valide bool) error {

	tab := ""
	for _, id := range list {
		tab = tab + strconv.FormatInt(id, 10) + ", "
	}

	sql := "update operation set valide = $1 where id in (" + tab + "0)"

	_, err := store.tx.Exec(sql, valide)

	if err != nil {
		return err
	}
	return nil
}

/*
Met à jour le champ annuler de la liste des opérations passées en paramétre
*/
func (store *dbStore) annulerOperations(list []int64, annuler bool) error {
	tab := ""
	for _, id := range list {
		tab = tab + strconv.FormatInt(id, 10) + ", "
	}

	sql := "update operation set annuler = $1 where id in (" + tab + "0)"

	_, err := store.db.Exec(sql, annuler)

	if err != nil {
		return err
	}
	return nil
}

func (store *dbStore) updateOperation(operation *Operation) error {

	strIdMoyen := "null"
	if operation.Moyen.Id != 0 {
		strIdMoyen = strconv.FormatInt(operation.Moyen.Id, 10)
	}

	sql := "update operation set dateOperation = $1, libelle = $2, description = $3, debit = $4, credit = $5, valide = $6, annuler = $7, idCompte = $8, idMoyen = " + strIdMoyen +
		", idLink = $9 where id = $10"

	_, err := store.tx.Exec(
		sql,
		operation.DateOperation, operation.Libelle, operation.Description,
		operation.Debit, operation.Credit, operation.Valide, operation.Annuler, operation.Compte.Id, operation.IdLink,
		operation.Id,
	)
	if err != nil {
		return err
	}
	return err
}

func (store *dbStore) soldeOperations(valide bool, idCompte int64) (float32, error) {

	var solde float32
	sql := "select coalesce(sum(credit), 0) - coalesce(sum(debit), 0) from operation where 1=1"

	if valide == true {
		sql = sql + " and valide = true"
	}

	if idCompte > 0 {
		sql = sql + " and idCompte = " + strconv.FormatInt(idCompte, 10)
	}

	sql = sql + " and annuler = false"

	err := store.db.Get(&solde, sql)
	if err != nil {
		return 0, err
	}

	return solde, nil
}

/*
*
Mise à jour des opérations avec idMoyen à null pour tous les idMoyens passés en parametre
*/
func (store *dbStore) deleteMoyenInOperation(idMoyen int64) error {

	sql := "update operation set idMoyen = null where idMoyen = $1"

	_, err := store.db.Exec(sql, idMoyen)
	if err != nil {
		return err
	}
	return err
}
