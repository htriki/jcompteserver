package main

import (
	"flag"
	"fmt"
	"jcompteserver/models"
	"log"
	"net/http"
	"os"
)

func main() {

	fmt.Println("Yo. Hocine (2023)")

	// Définir un paramètre nommé "env"
	env := flag.String("env", "", "Environnement d'exécution (ex: local, prod)")
	fileName := flag.String("file", "", "Fichier conf")

	flag.Parse()

	config, err := models.LoadConfig(*fileName, env)

	if err != nil {
		fmt.Fprintf(os.Stderr, "%s\n", err)
		os.Exit(1)
	}

	if err := run(&config); err != nil {
		fmt.Fprintf(os.Stderr, "%s\n", err)
		os.Exit(1)
	}

	config.Print()
}

// Fonction qui execute le server et qui va renvoyer l'erreur s'il y en a une
func run(config *models.DBConfig) error {

	srv := newServer()
	srv.store = &dbStore{} // association d'une instance dbStore à notre interface store
	err := srv.store.Open(config)

	if err != nil {
		return err
	}

	defer srv.store.Close()

	// Indique que la fonction qui s'occupe du "/" c'est la fonction serveHTTP de notre serveur
	http.HandleFunc("/", srv.serveHTTP)
	log.Printf("HTTP on port 9000")

	err = http.ListenAndServe(":9000", nil)
	if err != nil {
		return err
	}

	fmt.Printf("ok")

	return nil
}
