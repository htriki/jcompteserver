package main

import "log"

func (store *dbStore) findMoyen() ([]*Moyen, error) {

	var moyens []*Moyen
	sql := `select *  
			from moyen 
			order by libelle
			`

	err := store.db.Select(&moyens, sql)
	if err != nil {
		return moyens, err
	}

	return moyens, nil
}

func (store *dbStore) createMoyen(moyen *Moyen) error {

	query := "Insert into moyen (libelle) values ($1) RETURNING id"

	stmt, err := store.db.Prepare(query)
	if err != nil {
		return err
	}
	defer stmt.Close()
	var id int64
	err = stmt.QueryRow(moyen.Libelle).Scan(&id)
	if err != nil {
		return err
	}

	moyen.Id = id
	return nil
}

func (store *dbStore) updateMoyen(moyen *Moyen) error {

	sql := "update moyen set libelle = $1 where id = $2"

	_, err := store.db.Exec(
		sql,
		moyen.Libelle, moyen.Id,
	)

	if err != nil {
		return err
	}
	return err
}

func (store *dbStore) deleteMoyen(id int64) (int64, error) {

	log.Printf("--------------------------------------------------------- 1")
	_, err := store.db.Exec("delete from moyen where id = $1", id)
	log.Printf("--------------------------------------------------------- 2")
	if err != nil {
		return 0, err
	}

	log.Printf("--------------------------------------------------------- 3")
	return 0, nil

	// count, err := res.RowsAffected()

	// if err != nil {
	// 	log.Printf("deleteMoyen : la base de données ne retourne pas le nbre de ligne supprimée")
	// 	return 0, nil
	// }

	// return count, nil
}
