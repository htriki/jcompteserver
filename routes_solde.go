package main

import (
	"log"
	"net/http"
	"strconv"

	"github.com/gorilla/mux"
)

type soldes struct {
	All             float32 `json:"all"`
	AllValide       float32 `json:"allValide"`
	AllCompte       float32 `json:"allCompte"`
	AllCompteValide float32 `json:"allCompteValide"`
}

func (s *server) handleSolde() http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {

		solde, err := s.store.soldeOperations(false, -1)
		if err != nil {
			log.Printf("handleSolde : Pb de récupération du solde global. err : %v\n", err)
			s.respond(w, r, nil, http.StatusInternalServerError)
			return
		}

		s.respond(w, r, solde, http.StatusOK)
	}
}
func (s *server) handleSoldeValide() http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {

		solde, err := s.store.soldeOperations(true, -1)
		if err != nil {
			log.Printf("handleSoldeValide : Pb de récupération du solde global. err : %v\n", err)
			s.respond(w, r, nil, http.StatusInternalServerError)
			return
		}

		s.respond(w, r, solde, http.StatusOK)
	}
}

func (s *server) handleSoldeCompte() http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {

		vars := mux.Vars(r)
		idCompte, err := strconv.ParseInt(vars["id"], 10, 64) // 10 pour décimal et 64 car c'st un int64
		if err != nil {
			log.Printf("handleSoldeCompte : Pb de parse du parametre, err: %v", err)
			s.respond(w, r, nil, http.StatusBadRequest)
			return
		}

		solde, err := s.store.soldeOperations(false, idCompte)
		if err != nil {
			log.Printf("handleSoldeCompte : Pb de récupération du solde global. err : %v\n", err)
			s.respond(w, r, nil, http.StatusInternalServerError)
			return
		}

		s.respond(w, r, solde, http.StatusOK)
	}
}

func (s *server) handleSoldeValideCompte() http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {

		vars := mux.Vars(r)
		idCompte, err := strconv.ParseInt(vars["id"], 10, 64) // 10 pour décimal et 64 car c'st un int64
		if err != nil {
			log.Printf("handleSoldeValideCompte : Pb de parse du parametre, err: %v", err)
			s.respond(w, r, nil, http.StatusBadRequest)
			return
		}

		solde, err := s.store.soldeOperations(true, idCompte)
		if err != nil {
			log.Printf("handleSoldeValideCompte : Pb de récupération du solde global. err : %v\n", err)
			s.respond(w, r, nil, http.StatusInternalServerError)
			return
		}

		s.respond(w, r, solde, http.StatusOK)
	}
}

func (s *server) handleSoldeAll() http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {

		s.enableCors(&w)

		vars := mux.Vars(r)
		idCompte, err := strconv.ParseInt(vars["id"], 10, 64) // 10 pour décimal et 64 car c'st un int64
		if err != nil {
			log.Printf("handleSoldeAll : Pb de parse du parametre, err: %v", err)
			s.respond(w, r, nil, http.StatusBadRequest)
			return
		}

		all, err := s.store.soldeOperations(false, -1)
		if err != nil {
			log.Printf("handleSoldeAll : Pb de récupération solde err : %v\n", err)
			s.respond(w, r, nil, http.StatusInternalServerError)
			return
		}
		allValide, err := s.store.soldeOperations(true, -1)
		if err != nil {
			log.Printf("handleSoldeAll : Pb de récupération solde err : %v\n", err)
			s.respond(w, r, nil, http.StatusInternalServerError)
			return
		}
		allCompte, err := s.store.soldeOperations(false, idCompte)
		if err != nil {
			log.Printf("handleSoldeAll : Pb de récupération solde err : %v\n", err)
			s.respond(w, r, nil, http.StatusInternalServerError)
			return
		}
		allCompteValide, err := s.store.soldeOperations(true, idCompte)
		if err != nil {
			log.Printf("handleSoldeAll : Pb de récupération solde err : %v\n", err)
			s.respond(w, r, nil, http.StatusInternalServerError)
			return
		}

		soldes := soldes{
			All:             all,
			AllValide:       allValide,
			AllCompte:       allCompte,
			AllCompteValide: allCompteValide,
		}

		s.respond(w, r, soldes, http.StatusOK)
	}
}
