package main

import (
	"encoding/json"
	"log"
	"net/http"

	"github.com/gorilla/mux"
)

// pour intégrer gorilla mux j'ai tapé :
// go get github.com/gorilla/mux

const JWT_APP_KEY = "jcompte/Hocine/Triki"

type server struct {
	router *mux.Router
	store  Store
}

// Création d'un nouveau serveur qui contient un routeur
func newServer() *server {

	s := &server{ // création du serveur
		router: mux.NewRouter(), // création du router
	}
	s.routes() // configuration de toutes les routes

	return s
}

// Cette fonction délégue la reponse et la requete au router (Gorilla/Mux) dans le server
func (s *server) serveHTTP(w http.ResponseWriter, r *http.Request) {
	logRequestMiddleware(s.router.ServeHTTP).ServeHTTP(w, r)
}

// Réponse
func (s *server) respond(w http.ResponseWriter, _ *http.Request, data interface{}, status int) {
	w.Header().Add("Content-Type", "application/json")
	w.WriteHeader(status)

	if data == nil {
		return
	}

	err := json.NewEncoder(w).Encode(data)
	if err != nil {
		log.Printf("Pb avec le formattage JSON. err=%v\n", err)
	}

}

func (s *server) decode(w http.ResponseWriter, r *http.Request, data interface{}) error {
	return json.NewDecoder(r.Body).Decode(data)
}

// Regle les pb de CORS
func (s *server) enableCors(w *http.ResponseWriter) {
	(*w).Header().Set("Access-Control-Allow-Origin", "*")
	(*w).Header().Set("Access-Control-Allow-Methods", "GET, POST, PUT, OPTIONS, HEAD, DELETE, PATCH")
	(*w).Header().Set("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept")
	(*w).Header().Set("Access-Control-Allow-Credentials", "true")
}

func (s *server) manageError(err error, message string, w http.ResponseWriter, r *http.Request, status int) {

	log.Fatal(message)
	log.Fatal(err)
	s.respond(w, r, nil, status)
}

func (s *server) manageErrorWithRollback(err error, message string, w http.ResponseWriter, r *http.Request, status int) {

	if s.store.isTransactionExist() {
		s.store.Rollback()
	}
	s.manageError(err, message, w, r, status)
}
