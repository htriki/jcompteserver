package main

import (
	"database/sql"
	"jcompteserver/models"
	"log"

	"github.com/jmoiron/sqlx"
	//_ "github.com/mattn/go-sqlite3"
	_ "github.com/lib/pq"
)

type Store interface {
	Open(config *models.DBConfig) error // ouverture de la connexion
	Close() error                       // fermeture de la connexion
	BeginTx()                           // démarrage d'une transaction
	isTransactionExist() bool
	Commit() error
	Rollback() error

	findCompte() ([]*CompteComplete, error)
	findCompteActif() ([]*Compte, error)
	findCompteById(id int64) (*Compte, error)
	createCompte(compte *Compte) error
	deleteCompte(id int64) (int64, error)
	updateCompte(compte *Compte) error

	findTag() ([]*Tag, error)
	createTag(tag *Tag) error
	deleteTag(id int64) (int64, error)
	updateTag(tag *Tag) error

	findMoyen() ([]*Moyen, error)
	createMoyen(moyen *Moyen) error
	updateMoyen(moyen *Moyen) error
	deleteMoyen(id int64) (int64, error)

	findOperations() ([]*Operation, error)
	findOperationById(id int64) (Operation, error)
	findOperationByCompte(idCompte int64) ([]*Operation, error)
	countOperationByCompte(idCompte int64) (int, error)
	findOperationByCompteAndSearch(idCompte int64, value string) ([]*Operation, error)
	findLibelleBySearch(value string) ([]string, error)
	findLibelles() ([]string, error)
	createOperation(operation *Operation) error
	updateOperation(operation *Operation) error
	deleteOperation(id int64) (int64, error)
	valideOperations([]int64, bool) error
	annulerOperations([]int64, bool) error
	soldeOperations(bool, int64) (float32, error)
	deleteMoyenInOperation(int64) error // suppression du moyen passé en parametre sur toutes les opérations
	//dupliquerOperation(idFrom int64, dateOperation time.Time) error

	createOpTag(opTag *OpTag) error
	deleteOpTagByOperation(idOperation int64) (int64, error)
	deleteOpTag4Tag(idTag int64) (int64, error)
	removeOpTagForOperations(inTag int64) (int64, error)
}

type dbStore struct {
	db *sqlx.DB
	tx *sql.Tx
}

func (store *dbStore) Open(config *models.DBConfig) error {

	//db, err := sqlx.Connect("sqlite3", "jcompte.db")
	db, err := sqlx.Connect("postgres", config.GetConnectionString())
	if err != nil {
		return err
	}

	log.Println("Désormais, connecté à la base de données")
	log.Println("La base doit etre créée")
	//log.Println("Création du schema")
	//db.MustExec(schema)
	log.Println("Schema : OK")
	store.db = db
	return nil
}

func (store *dbStore) Close() error {

	log.Println("Tentative de déconnexion de la base de données")
	return store.db.Close()
}

func (store *dbStore) BeginTx() {

	log.Println("Démarrage d'une transaction")
	tx, err := store.db.Begin()
	if err != nil {
		// TODO
	}

	store.tx = tx
}

func (store *dbStore) Commit() error {

	log.Println("Commit de la transaction")
	err := store.tx.Commit()
	store.tx = nil
	return err
}

func (store *dbStore) Rollback() error {

	log.Println("Rollback de la transaction")
	err := store.tx.Rollback()
	store.tx = nil
	return err
}

func (store *dbStore) isTransactionExist() bool {
	return store.tx != nil
}
