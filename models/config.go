package models

import (
	"errors"
	"fmt"
	"os"

	"gopkg.in/yaml.v2"
)

type DBConfig struct {
	Host     string `yaml:"host"`
	User     string `yaml:"user"`
	Password string `yaml:"password"`
	DBName   string `yaml:"dbname"`
	SSLMode  string `yaml:"sslmode"`
}

type Config struct {
	Local DBConfig `yaml:"local"`
	Prod  DBConfig `yaml:"prod"`
}

func LoadConfig(fileName string, env *string) (DBConfig, error) {

	file, err := os.ReadFile(fileName)
	if err != nil {
		return DBConfig{}, fmt.Errorf("Erreur : Lecture du fichier de configuration : %v", err)
	}

	var config Config
	err = yaml.Unmarshal(file, &config)
	if err != nil {
		return DBConfig{}, fmt.Errorf("Erreur : Parsing du fichier YAML : %v", err)
	}

	if *env == "" {
		return DBConfig{}, errors.New("Erreur : Le paramètre '-env' est obligatoire.")
	}

	switch *env {
	case "local":
		return config.Local, nil
	case "prod":
		return config.Prod, nil
	default:
		return DBConfig{}, fmt.Errorf("Erreur : Environnement inconnu : %s", env)
	}
}

func (c *DBConfig) Print() {
	fmt.Println("------------------------------")
	fmt.Println("- configuration")
	fmt.Println("- host : ", c.Host)
	fmt.Println("- db   : ", c.DBName)
	fmt.Println("- user : ", c.User)
	fmt.Println("- mode : ", c.SSLMode)
	fmt.Println("------------------------------")
}

func (c *DBConfig) GetConnectionString() string {

	return fmt.Sprintf("host=%s user=%s password=%s dbname=%s sslmode=%s", c.Host, c.User, c.Password, c.DBName, c.SSLMode)
}
