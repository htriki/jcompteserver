package main

import (
	"log"
	"net/http"
	"strconv"

	"github.com/gorilla/mux"
)

type jsonTag struct {
	Id      int64  `json:"id"`
	Libelle string `json:"libelle"`
	Couleur string `json:"couleur"`
}

func (s *server) handleTag() http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {

		s.enableCors(&w)

		tags, err := s.store.findTag()
		if err != nil {
			log.Printf("Pb de récupération des tags. err=%v\n", err)
			s.respond(w, r, nil, http.StatusInternalServerError)
			return
		}

		var resp = make([]jsonTag, len(tags))
		for i, t := range tags {
			resp[i] = mapTagToJson(t)
		}

		s.respond(w, r, resp, http.StatusOK)

	}
}

func (s *server) handleTagCreate() http.HandlerFunc {

	return func(w http.ResponseWriter, r *http.Request) {

		s.enableCors(&w)

		jst := jsonTag{}
		err := s.decode(w, r, &jst)
		if err != nil {
			log.Printf("Impossible de parse le tag %v", err)
			s.respond(w, r, nil, http.StatusBadRequest)
			return
		}

		// Créer un tag
		t := &Tag{
			Id:      0,
			Libelle: jst.Libelle,
			Couleur: jst.Couleur,
		}

		// Sauvegarde du tag
		err = s.store.createTag(t)
		if err != nil {
			log.Printf("Impossible de créer le tag err=%v", err)
			s.respond(w, r, nil, http.StatusInternalServerError)
			return
		}

		var resp = mapTagToJson(t)
		s.respond(w, r, resp, http.StatusOK)
	}
}

func (s *server) handleTagUpdate() http.HandlerFunc {

	return func(w http.ResponseWriter, r *http.Request) {

		s.enableCors(&w)

		req := jsonTag{}
		err := s.decode(w, r, &req)
		if err != nil {
			log.Printf("Impossible de parse le Tag %v", err)
			s.respond(w, r, nil, http.StatusBadRequest)
			return
		}

		// Création d'une opération
		tg := &Tag{
			Id:      req.Id,
			Libelle: req.Libelle,
			Couleur: req.Couleur,
		}

		// Mise à jour de l'opération
		err = s.store.updateTag(tg)
		if err != nil {
			log.Printf("Impossible de mettre à jour le Tag err=%v", err)
			s.respond(w, r, nil, http.StatusInternalServerError)
			return
		}

		var resp = mapTagToJson(tg)
		s.respond(w, r, resp, http.StatusOK)
	}
}

/*
---------------------------------------------------------------------------------------
Avant la suppression d'un tag il faut supprimer les optag liés
---------------------------------------------------------------------------------------
*/
func (s *server) handleTagDelete() http.HandlerFunc {

	return func(w http.ResponseWriter, r *http.Request) {

		s.enableCors(&w)

		vars := mux.Vars(r)
		idTag, err := strconv.ParseInt(vars["id"], 10, 64) // 10 pour décimal et 64 car c'st un int64
		if err != nil {
			log.Printf("Pb de parse du parametre")
			s.respond(w, r, nil, http.StatusBadRequest)
			return
		}

		count, err := s.store.deleteOpTag4Tag(idTag)
		if err != nil {
			log.Printf("Erreur lors de la suppression des optag pour le tag : %v. err=%v\n", idTag, err)
			s.respond(w, r, nil, http.StatusInternalServerError)
			return
		}
		log.Printf("nombre d'optag supprimés : %v", count)

		count, err = s.store.deleteTag(idTag)
		if err != nil {
			log.Printf("Erreur lors de la suppression du tag : %v. err=%v\n", idTag, err)
			s.respond(w, r, nil, http.StatusInternalServerError)
			return
		}

		log.Printf("nombre de ligne supprimée : %v", count)

		s.respond(w, r, count, http.StatusOK)
	}
}

func mapTagToJson(t *Tag) jsonTag {
	return jsonTag{
		Id:      t.Id,
		Libelle: t.Libelle,
		Couleur: t.Couleur,
	}
}
