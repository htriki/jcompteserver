package main

import (
	"database/sql"
	"time"
)

type Compte struct {
	Id        int64  `db:"id"`
	Libelle   string `db:"libelle"`
	Titulaire string `db:"titulaire"`
	Numero    string `db:"numero"`
	Actif     bool   `db:"actif"`
}

type CompteComplete struct {
	Id        int64   `db:"id"`
	Libelle   string  `db:"libelle"`
	Titulaire string  `db:"titulaire"`
	Numero    string  `db:"numero"`
	Actif     bool    `db:"actif"`
	Total     float32 `db:"total"`
	TotalReel float32 `db:"totalreel"`
}

type Tag struct {
	Id      int64  `db:"id"`
	Libelle string `db:"libelle"`
	Couleur string `db:"couleur"`
}

type Moyen struct {
	Id      int64  `db:"id"`
	Libelle string `db:"libelle"`
}

type Operation struct {
	Id            int64     `db:"id"`
	DateOperation time.Time `db:"dateOperation"`
	Libelle       string    `db:"libelle"`
	Description   string    `db:"libelle"`
	Debit         float32   `db:"debit"`
	Credit        float32   `db:"credit"`
	Valide        bool      `db:"valide"`
	Annuler       bool      `db:"annuler"`
	IdLink        int64     `db:"idlink"`
	Compte        Compte
	Tags          []Tag
	Moyen         Moyen
}

type OpTag struct {
	Id          int64 `db:"id"`
	IdOperation int64 `db:"idOperation"`
	IdTag       int64 `db:"idTag"`
}

type OperationRecord struct {
	IdOperation      int64          `db:"idOperation"`
	DateOperation    time.Time      `db:"dateOperation"`
	LibelleOperation sql.NullString `db:"libelleOperation"`
	Description      sql.NullString `db:"libelle"`
	Debit            float32        `db:"debit"`
	Credit           float32        `db:"credit"`
	Valide           bool           `db:"valide"`
	Annuler          bool           `db:"annuler"`
	IdCompte         int64          `db:"idCompte"`
	LibelleCompte    string         `db:"libelleCompte"`
	TitulaireCompte  string         `db:"titulaireCompte"`
	NumeroCompte     string         `db:"numeroCompte"`
	ActifCompte      bool           `db:"actifCompte"`
	IdTag            sql.NullInt64  `db:"idTag"`
	LibelleTag       sql.NullString `db:"libelleTag"`
	CouleurTag       sql.NullString `db:"couleurTag"`
	IdMoyen          sql.NullInt64  `db:"idMoyen"`
	LibelleMoyen     sql.NullString `db:"libelleMoyen"`
	IdLink           sql.NullInt64  `db:"idlink"`
}

/*
func (c Compte) CompteString() string {
	return fmt.Sprintf("id=%v, libelle=%v", c.Id, c.Libelle)
}

func (t Tag) TagString() string {
	return fmt.Sprintf("id=%v, libelle=%v, couleur=%v", t.Id, t.Libelle, t.Couleur)
} */
