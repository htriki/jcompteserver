package main

import "net/http"

/*
contient toutes les routes
*/
func (s *server) routes() {

	s.router.HandleFunc("/", s.handleIndex()).Methods("GET")
	s.router.HandleFunc("/api/token", s.handleTokenCreate()).Methods("POST")

	// WS : Hello  ---------------------------------------------------------------------------------------
	s.router.HandleFunc("/api/hello", s.handleOptions()).Methods(http.MethodOptions)
	s.router.HandleFunc("/api/hello", s.handleHello()).Methods("GET")

	// WS : Compte ---------------------------------------------------------------------------------------
	s.router.HandleFunc("/api/compte/{id:[0-9]+}", s.handleOptions()).Methods(http.MethodOptions)
	s.router.HandleFunc("/api/compte", s.handleOptions()).Methods(http.MethodOptions)
	s.router.HandleFunc("/api/compte/actif", s.handleOptions()).Methods(http.MethodOptions)

	s.router.HandleFunc("/api/compte/{id:[0-9]+}", s.handleCompteDetail()).Methods("GET")    // Lecture d'un compte
	s.router.HandleFunc("/api/compte/{id:[0-9]+}", s.handleCompteDelete()).Methods("DELETE") // Suppression d'un compte
	s.router.HandleFunc("/api/compte", s.handleCompte()).Methods("GET")                      // Lecture de tous les comptes
	s.router.HandleFunc("/api/compte", s.handleCompteCreate()).Methods("POST")               // Création d'un compte
	s.router.HandleFunc("/api/compte", s.handleCompteUpdate()).Methods(http.MethodPut)       // Modification d'un compte
	s.router.HandleFunc("/api/compte/actif", s.handleCompteActif()).Methods("GET")           // Lecture de tous les comptes actifs

	// WS : Tag ------------------------------------------------------------------------------------------
	s.router.HandleFunc("/api/tag/{id:[0-9]+}", s.handleOptions()).Methods(http.MethodOptions)
	s.router.HandleFunc("/api/tag", s.handleOptions()).Methods(http.MethodOptions)

	s.router.HandleFunc("/api/tag/{id:[0-9]+}", s.handleIndex()).Methods("GET")        // Lecture d'un Tag
	s.router.HandleFunc("/api/tag/{id:[0-9]+}", s.handleTagDelete()).Methods("DELETE") // Suppression d'un Tag
	s.router.HandleFunc("/api/tag", s.handleTag()).Methods("GET")                      // Lecture de tous les Tags
	s.router.HandleFunc("/api/tag", s.handleTagCreate()).Methods("POST")               // Création d'un Tag
	s.router.HandleFunc("/api/tag", s.handleTagUpdate()).Methods(http.MethodPut)       // Mise à jour d'un Tag

	// WS : Moyen ------------------------------------------------------------------------------------------
	s.router.HandleFunc("/api/moyen/{id:[0-9]+}", s.handleOptions()).Methods(http.MethodOptions)
	s.router.HandleFunc("/api/moyen", s.handleOptions()).Methods(http.MethodOptions)

	s.router.HandleFunc("/api/moyen", s.handleMoyen()).Methods("GET")                      // Lecture de tous les Moyens
	s.router.HandleFunc("/api/moyen", s.handleMoyenCreate()).Methods("POST")               // Création d'un Moyen
	s.router.HandleFunc("/api/moyen", s.handleMoyenUpdate()).Methods("PUT")                // Mise à jour d'un Moyen
	s.router.HandleFunc("/api/moyen/{id:[0-9]+}", s.handleMoyenDelete()).Methods("DELETE") // Suppression d'un Moyen

	// WS : Opération ------------------------------------------------------------------------------------
	s.router.HandleFunc("/api/operation/{id:[0-9]+}", s.handleOptions()).Methods(http.MethodOptions)
	s.router.HandleFunc("/api/operation", s.handleOptions()).Methods(http.MethodOptions)
	s.router.HandleFunc("/api/operation/valide", s.handleOptions()).Methods(http.MethodOptions)
	s.router.HandleFunc("/api/operation/unvalide", s.handleOptions()).Methods(http.MethodOptions)
	s.router.HandleFunc("/api/operation/cancel", s.handleOptions()).Methods(http.MethodOptions)
	s.router.HandleFunc("/api/operation/uncancel", s.handleOptions()).Methods(http.MethodOptions)
	s.router.HandleFunc("/api/operation/search", s.handleOptions()).Methods(http.MethodOptions)
	s.router.HandleFunc("/api/operation/libelles", s.handleOptions()).Methods(http.MethodOptions)
	s.router.HandleFunc("/api/operation/dupliquer", s.handleOptions()).Methods(http.MethodOptions)

	s.router.HandleFunc("/api/operation/{id:[0-9]+}", s.handleOperationDetail()).Methods("GET")                           // Détail d'une opération
	s.router.HandleFunc("/api/operation/{id:[0-9]+}", s.handleOperationDelete()).Methods("DELETE")                        // Suppression d'une opération
	s.router.HandleFunc("/api/operation", s.handleOperation()).Methods("GET")                                             // Récupération de toutes les opérations
	s.router.HandleFunc("/api/operation", s.handleOperationCreate()).Methods(http.MethodPost)                             // Création d'une opération
	s.router.HandleFunc("/api/operation", s.handleOperationUpdate()).Methods(http.MethodPut)                              // Mise à jour d'une opération
	s.router.HandleFunc("/api/operation/compte/{idCompte:[0-9]+}", s.handleOperationByCompte()).Methods("GET")            // Lecture opération pour 1 compte
	s.router.HandleFunc("/api/operation/compte", s.handleOperationByCompteAndSearch()).Methods("POST")                    // Lecture opération 1 compte + recherche valeur
	s.router.HandleFunc("/api/operation/count/compte/{idCompte:[0-9]+}", s.handleCountOperationByCompte()).Methods("GET") // Lecture du nombre opération pour 1 compte
	s.router.HandleFunc("/api/operation/valide", s.handleOperationValide(true)).Methods("PUT")                            // validation d'une opération
	s.router.HandleFunc("/api/operation/unvalide", s.handleOperationValide(false)).Methods("PUT")                         // dévalidation d'une opération
	s.router.HandleFunc("/api/operation/cancel", s.handleOperationCancel(true)).Methods("PUT")                            // annulation d'une opération
	s.router.HandleFunc("/api/operation/uncancel", s.handleOperationCancel(false)).Methods("PUT")                         // désannulation d'une opération
	s.router.HandleFunc("/api/operation/search/{search}", s.handleOperationSearchLibelle()).Methods("GET")                // Recherche d'un libellé dans une opération
	s.router.HandleFunc("/api/operation/libelles", s.handleOperationLibelles()).Methods("GET")                            // Renvoie la liste de tous les libellés
	s.router.HandleFunc("/api/operation/dupliquer", s.handleOperationDupliquer()).Methods("POST")                         // Dupliquer des opérations à une date donnée

	// WS : Solde -----------------------------------------------------------------------------------------
	s.router.HandleFunc("/api/operation/solde", s.handleSolde()).Methods("GET")                               // Solde total
	s.router.HandleFunc("/api/operation/soldevalide", s.handleSoldeValide()).Methods("GET")                   // Solde total mais uniquement les validés
	s.router.HandleFunc("/api/operation/solde/{id:[0-9]+}", s.handleSoldeCompte()).Methods("GET")             // Solde total pour un compte donné
	s.router.HandleFunc("/api/operation/soldevalide/{id:[0-9]+}", s.handleSoldeValideCompte()).Methods("GET") // Solde validé pour un compte donné
	s.router.HandleFunc("/api/operation/solde/all/{id:[0-9]+}", s.handleSoldeAll()).Methods("GET")            // Solde validé pour un compte donné
}
