 CREATE TABLE IF NOT EXISTS compte (
    id serial PRIMARY KEY,
    libelle TEXT NOT NULL,
    titulaire TEXT NOT NULL,
    numero TEXT,
    actif BOOLEAN DEFAULT true
);

CREATE TABLE IF NOT EXISTS tag (
    id serial PRIMARY KEY,
    libelle TEXT NOT NULL,
    couleur TEXT
);


CREATE TABLE IF NOT EXISTS moyen (
    id serial PRIMARY KEY,
    libelle TEXT NOT NULL
);

CREATE TABLE IF NOT EXISTS operation (
    id serial PRIMARY KEY,
    dateOperation DATE NOT NULL,
    libelle TEXT,
    description TEXT,
    debit NUMERIC(8,2),
    credit NUMERIC(8,2),
    valide BOOLEAN DEFAULT FALSE,
    annuler BOOLEAN DEFAULT FALSE,
    idCompte INTEGER NOT NULL,
    idMoyen INTEGER,
    idLink INTEGER,
    FOREIGN KEY(idCompte) REFERENCES compte(id),
    FOREIGN KEY(idMoyen)  REFERENCES moyen(id)
);

CREATE TABLE IF NOT EXISTS optag (
    id serial PRIMARY KEY,
    idOperation INTEGER,
    idTag INTEGER,
    FOREIGN KEY(idOperation) REFERENCES operation(id),
    FOREIGN KEY(idTag) REFERENCES tag(id)
);


-- Contraintes ajoutées
ALTER TABLE compte ADD CONSTRAINT compte_unique_numero UNIQUE (numero);
ALTER TABLE compte ADD CONSTRAINT compte_unique_libelle_titulaire_numero UNIQUE (libelle, titulaire, numero);
--
ALTER TABLE moyen ADD CONSTRAINT moyen_unique_libelle UNIQUE (libelle);
--
ALTER TABLE tag ADD CONSTRAINT tag_unique_libelle UNIQUE (libelle);

ALTER TABLE optag ADD CONSTRAINT optag_unique UNIQUE (idoperation, idtag);
