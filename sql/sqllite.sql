-- SQLite
select * from moyen;
delete from moyen;
update compte set Titulaire= 'Hocine & Jeanne', Numero = '321353215';

select * from operation;
delete from operation;

select * from sqlite_sequence;
update sqlite_sequence set seq = 0
delete from compte;
insert into operation (dateOperation, libelle, description, debit, valide,idCompte)
values (date(), 'zzzz', 'blabla', 22, true, 2);

select o.id as idOperation, o.dateOperation,  o.libelle as libelleOperation, o.description, 
			coalesce(o.debit, 0) as debit, 
			coalesce(o.credit, 0) as credit, 
			o.valide, o.idCompte, 
			c.libelle as libelleCompte,
			t.id as idTag,
			t.libelle as libelleTag,
			t.couleur as couleurTag
			from operation o 
			inner join compte c on o.idCompte = c.id
			left join optag ot on o.id = ot.idOperation
			left join tag t on t.id = ot.idTag;

select id, debit, credit from operation;
update operation set valide = false where id in (1, 2)
select * from opTag;
select * from operation
update operation set valide = false where id in (1, 2, 3, 0)


select sum(coalesce(credit, 0)) - sum(coalesce(debit, 0)) from operation;
select coalesce(sum(credit), 0) - coalesce(sum(debit), 0) from operation;

select c.* , 
(select coalesce(sum(credit) - sum(debit), 0) from operation where idCompte = c.id) as total,
(select coalesce(sum(credit) - sum(debit), 0) from operation where idCompte = c.id and valide=true) as totalReel
from compte c;

select coalesce(sum(credit) - sum(debit), 0) from operation where idCompte = 9
select * from moyen;

insert into moyen (libelle) values ('Espéce');
select * from operation;
delete from operation;
select * from compte;
delete from compte where id = 1;

PRAGMA foreign_keys=on;

select o.id as idOperation, o.dateOperation,  o.libelle as libelleOperation, o.description, 
			coalesce(o.debit, 0) as debit, 
			coalesce(o.credit, 0) as credit, 
			o.valide, o.idCompte, o.idMoyen,
			c.libelle as libelleCompte,
			t.id as idTag,
			t.libelle as libelleTag,
			t.couleur as couleurTag,
			m.libelle as libelleMoyen
			from operation o 
			inner join compte c on o.idCompte = c.id
			left join optag ot on o.id = ot.idOperation
			left join tag t on t.id = ot.idTag
			left join moyen m on m.id = o.idMoyen
			order by o.id

			update operation set idMoyen = 1

-- ----------------------------------------------------
-- Moyens
-- ----------------------------------------------------
insert into moyen (libelle) values ('Chèque');
insert into moyen (libelle) values ('Carte Bleue');
insert into moyen (libelle) values ('Virement');
insert into moyen (libelle) values ('Virement compte à compte');

select count(*) from operation where idCompte = 4


select o.id as idOperation, o.dateOperation,  o.libelle as libelleOperation, o.description, 
coalesce(o.debit, 0) as debit, coalesce(o.credit, 0) as credit, 
o.valide, o.idCompte, c.libelle as libelleCompte,
c.titulaire as titulaireCompte, c.numero as numeroCompte,
c.actif as actifCompte, t.id as idTag,
t.libelle as libelleTag, t.couleur as couleurTag,
o.idMoyen, m.libelle as libelleMoyen
from operation o 
inner join compte c on o.idCompte = c.id
left join optag ot on o.id = ot.idOperation
left join tag t on t.id = ot.idTag
left join moyen m on m.id = o.idMoyen  
where c.id = 1
and ( 
	o.libelle like '%ali%'
	or o.description like '%ali%'
	or o.id in (
		select x.idOperation from optag x
		inner join tag y on x.idTag = y.id 
		where y.libelle like '%ali%'	
	)
	or m.libelle like '%ali%'
)
order by o.dateOperation, o.id;

